# Matboj-Regelwerk

Das Regelwerk für den Matboj, der 2021 und 2022 auf dem Mathecamp stattfand.


# Dokument / Download
Die [aktuelle Version][pdf] des Regelwerks wird aus diesem Repository automatisch (mittels GitLab Runner) kompiliert.

Der [Log][log] des letzten Kompiliervorgangs kann ebenfalls eingesehen werden.

# Technisches
(Nur relevant für Betreur:innen)

Es gab mal ein Matboj repository (im Sommer 2022), das alle Materialien für den Matboj enthielt
(Aufgabensammlung etc).

Da alle Materialien des Mathezirkels mittlerweile open source sein müssen,
wurde dieses zu
[kesslermaximilian/mathecamp-matboj](https://gitlab.com/kesslermaximilian/mathecamp-matboj)
migriert.
Das repository ist weiterhin privat, meldet euch bei Max, wenn Ihr Zugang braucht.

Das ist nicht schön, aber wir haben derzeit keine bessere Lösung.
Hier befindet sich zur Zeit nur noch das Regelwerk.

# License

Source files within this repository are licensed under MIT, provided PDFs are licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by/4.0/). See [LICENSE.md](LICENSE.md) for further details.


[pdf]: https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.pdf
[Log]: https://mathezirkel.gitlab.io/content/matboj-regelwerk/Matboj_Regelwerk.log

