Different licenses apply to different parts of this repository

1. All files present in this repository are licensed under MIT
2. The compiled pdf, obtained by running `make`, is additionally licensed under the Creative Commons Share-Alike 4.0 international license

You can find the corresponding licenses in the subfolder `licenses`
